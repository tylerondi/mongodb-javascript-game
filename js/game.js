/* game namespace */
var game = {

    /** 
    * an object where to store game global data
    */

    data: {
        score: 0
    },

    // Run on page load.
    "onload": function () {

        // Initialize the video.
        if (!me.video.init("screen", me.video.CANVAS, 640, 480, true, 'auto')) {
            alert("Your browser does not support HTML5 canvas.");
            return;
        }

        // add "#debug" to the URL to enable the debug Panel
        if (document.location.hash === "#debug") {
            window.onReady(function () {
                me.plugin.register.defer(this, me.debug.Panel, "debug", me.input.KEY.V);
            });
        }

        // Initialize the audio.
        me.audio.init("mp3,ogg");

        // Set a callback to run when loading is complete.
        me.loader.onload = this.loaded.bind(this);

        // Load the resources.
        me.loader.preload(game.resources);

        // Initialize melonJS and display a loading screen.
        me.state.change(me.state.LOADING);
    },

    /* ---
     
    callback when everything is loaded
     
    ---  */
 
    "loaded": function () {
        // set the "Play/Ingame" Screen Object
        me.state.set(me.state.MENU, new game.TitleScreen());

        // set the "Play/Ingame" Screen Object
        me.state.set(me.state.PLAY, new game.PlayScreen());

        // set a global fading transition for the screen
        me.state.transition("fade", "#FFFFFF", 250);

        // register our player entity in the object pool
        me.pool.register("mainPlayer", game.PlayerEntity);
        me.pool.register("CoinEntity", game.CoinEntity);
        me.pool.register("EnemyEntity", game.EnemyEntity);

        // enable the keyboard
        me.input.bindKey(me.input.KEY.LEFT, "left");
        me.input.bindKey(me.input.KEY.RIGHT, "right");
        me.input.bindKey(me.input.KEY.UP, "jump", true);

        // display the menu title
        me.state.change(me.state.MENU);
    }


};

//game resources
game.resources = [
  /**
  * Graphics.
  */
  // our title screen
  { name: "title_screen", type: "image", src: "data/img/gui/title_screen.jpg" },
  // our level tileset
  { name: "area01_level_tiles", type: "image", src: "data/img/map/area01_level_tiles.png" },
  { name: "Items", type: "image", src: "data/img/map/Items.png" },
  { name: "Door", type: "image", src: "data/img/map/Door.png" },
  // the main player spritesheet
  { name: "gripe_run_right", type: "image", src: "data/img/sprite/gripe_run_right.png" },
  // the parallax background
  { name: "area01_bkg0", type: "image", src: "data/img/area01_bkg0.jpg" },
  { name: "area01_bkg1", type: "image", src: "data/img/area01_bkg1.png" },
  // the spinning coin spritesheet
  { name: "spinning_coin_gold", type: "image", src: "data/img/sprite/spinning_coin_gold.png" },
  // our enemty entity
  { name: "wheelie_right", type: "image", src: "data/img/sprite/wheelie_right.png" },
  // game font
  { name: "32x32_font", type: "image", src: "data/img/font/32x32_font.png" },

  /* 
  * Maps. 
  */
  { name: "area01", type: "tmx", src: "data/map/area01.tmx" },
  { name: "area02", type: "tmx", src: "data/map/area02.tmx" },
  { name: "area03", type: "tmx", src: "data/map/area03.tmx" },


  /* 
  * Background music. 
  */
  { name: "dst-inertexponent", type: "audio", src: "data/bgm/" },

  /* 
  * Sound effects. 
  */
  { name: "cling", type: "audio", src: "data/sfx/" },
  { name: "stomp", type: "audio", src: "data/sfx/" },
  { name: "jump", type: "audio", src: "data/sfx/" }
];